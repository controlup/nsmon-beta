# ControlUp NetScaler Monitor - Beta

NetScaler monitor is a service based, web-style application for real-time monitoring of your NetScalers.
NetScaler Monitor will allow you to get the following real-time metrics -

  - Overall status - such as info about the running config, version, etc - and resource consumption of your NetScalers
  - Drill down view into load balancers to see the status and load of the services, service groups and service members
  - Status of the gateways configured on the NetScalers and drill down to status of the related STAs
  - Searchable list of HDX sessions running through the NetScalers

### Installation

Get it here - [download](https://www.controlup.com/controlup-netscaler/).

Run the setup file. It will install a `Control NetScaler Monitor` service.

Once setup is done, browse (Chrome and Firefox only for now) to http://localhost:3333.

Once it opened, go to Settings menu and add your appliances.